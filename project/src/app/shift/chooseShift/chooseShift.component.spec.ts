import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseShiftComponent } from './chooseShift.component';

describe('ChooseShiftComponent', () => {
  let component: ChooseShiftComponent;
  let fixture: ComponentFixture<ChooseShiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChooseShiftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseShiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
