import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ShiftService } from '../../services/shift.service';
import { Data } from '../../Models/data.component';
import { selectShift } from '../../Models/selectShift.component';
import { concat } from 'rxjs';
import { LoginService } from '../../services/login.service';
import { City, ShiftKind } from '../../Models/classes';
import { IdName } from '../../Models/IdName.component';
//import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-chooseShift',
  templateUrl: './chooseShift.component.html',
  styleUrls: ['./chooseShift.component.css']
})
export class ChooseShiftComponent implements OnInit {



  @Output()
  seccess = new EventEmitter<string>();

  CheckBox:boolean=true;
  data: Data = null;
  select: selectShift = new selectShift();

  constructor(private shiftService: ShiftService, private loginService: LoginService) { }

  ngOnInit() {
    this.GetData();
  }

  GetData() {
    this.shiftService.GetData(this.loginService.CurrentUser.Id).subscribe(x => {
      console.log(x);
      this.data = x;
    });
  }
  searchShift() {
    console.log(this.CheckBox)
    
    console.log("searchShift")
    if (this.data.Volunteers == null||this.CheckBox) {
      this.select.User = new IdName();
      console.log(this.select)
      this.select.User.Id = this.loginService.CurrentUser.Id;
    }
    this.shiftService.searchShift(this.select).subscribe(x => {
      console.log(x)
      this.seccess.emit('complete');
    }
    )
  }
}
