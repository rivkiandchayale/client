import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftStepperComponent } from './shiftStepper.component';

describe('ShiftComponent', () => {
  let component: ShiftStepperComponent;
  let fixture: ComponentFixture<ShiftStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
