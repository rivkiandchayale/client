import { Component, OnInit } from '@angular/core';
import { MatStep, MatStepper } from '@angular/material';

@Component({
  selector: 'app-shift-stepper',
  templateUrl: './shiftStepper.component.html',
  styleUrls: ['./shiftStepper.component.css'],
})
export class ShiftStepperComponent implements OnInit {

  stepper: MatStepper;

  constructor() { }

  ngOnInit() {

  }

  CurrentStep(stepper: MatStepper) {
    this.stepper = stepper;
  }
  NextStep()
  {
    this.stepper.next();
  }
}
