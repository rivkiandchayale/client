import {MatButtonModule, MatCheckboxModule,MatTableModule, MatDatepickerModule,  MatStepperModule, MatSnackBarModule,
  MatTabsModule, MatRadioModule, MatFormFieldModule, MatInputModule, MatSlideToggleModule, MatPaginatorModule, MatIconModule, MatToolbarModule, MatNativeDateModule, MatCardModule, MatSortModule, MatSelectModule, MatSidenavModule} from '@angular/material';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule,MatTableModule, MatDatepickerModule,  MatStepperModule, MatSnackBarModule,
    MatTabsModule, MatRadioModule, MatFormFieldModule, MatInputModule, MatSlideToggleModule, MatPaginatorModule, MatIconModule, MatToolbarModule, MatNativeDateModule, MatCardModule, MatSortModule, MatSelectModule, MatSidenavModule],
  exports: [MatButtonModule, MatCheckboxModule,MatTableModule, MatDatepickerModule,  MatStepperModule, MatSnackBarModule,
    MatTabsModule, MatRadioModule, MatFormFieldModule, MatInputModule, MatSlideToggleModule, MatPaginatorModule, MatIconModule, MatToolbarModule, MatNativeDateModule, MatCardModule, MatSortModule, MatSelectModule, MatSidenavModule],
})
export class MaterialModule { }