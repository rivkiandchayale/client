import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule  } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EventComponent } from './event/event/event.component';
import { ChooseShiftComponent } from './shift/chooseShift/chooseShift.component';
import { EquipmentComponent } from './equipment/equipment.component';
import { ShiftStepperComponent } from './shift/shiftStepper/shiftStepper.component';
import { AssignmentComponent } from './assignment/assignment.component';
import {VolunteerFormComponent} from './volunteer/volunteer-form/volunteer-form.component'
import { VolunteerListComponent } from './volunteer/volunteer-list/volunteer-list.component';
import { AmbulanceListComponent } from './ambulance/ambulance-list/ambulance-list.component';
import { AmbulanceFormComponent } from './ambulance/ambulance-form/ambulance-form.component';
import { ErrorComponent } from './error/error.component';
import { EventListComponent } from './event/event-list/event-list.component';
const routing:Routes=[
  {
    path:'',
    component:LoginComponent  
  },
  // {
  //   path:'**',
  //   component:ErrorComponent
  // },
  {
    path:'volunteerForm/:id',
    component:VolunteerFormComponent  
  },
  {
    path:'ambulanceForm/:id',
    component:AmbulanceFormComponent  
  },
  {
    path:'assignment',
    component:AssignmentComponent  
  },
  {
    path:'login',
    component:LoginComponent  
  },
  {
    path:'event',
    component:EventComponent  
  },
  {
    path:'shift',
    component:ShiftStepperComponent
  },
  {
    path:'chooseShift',
    component:ChooseShiftComponent  
  },
  {
    path:'equipment',
    component:EquipmentComponent  
  },
  {
    path:'volunteerList',
    component:VolunteerListComponent  
  },
  {
    path:'ambulanceList',
    component:AmbulanceListComponent
  },
  {
    path:'eventList',
    component:EventListComponent
  }  
];

@NgModule({
  imports: [RouterModule.forRoot(routing)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
