import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { User } from '../Models/classes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  userExist: boolean = false;
  hide:boolean=true;
  constructor(private loginService: LoginService, private router: Router) {

  }

  ngOnInit() {
  }


  checkUserExist() {
    this.loginService.GetVolunteerByPassword(this.user).subscribe(
      x => {
        if (x) {
          this.loginService.CurrentUser = x;
          if (x.IsVolunteer)
            this.router.navigate(['/assignment']);
            //route to ציוד
           // this.router.navigate([''])
        }
        this.userExist = true;
      }
    );

  }
}
