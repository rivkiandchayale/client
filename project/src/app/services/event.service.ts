import { Injectable } from '@angular/core';
import { AppSettings } from '../appSetting.component';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class EventService {

  ApiBaseRoute = AppSettings.API_ENDPOINT + "Event";
  constructor(private http: HttpClient) { }

  GetListOfEvents(): Observable<any> {
    return this.http.get<any>(`${this.ApiBaseRoute}/GetListOfEvents`);
  }

}
