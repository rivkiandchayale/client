import { Injectable } from '@angular/core';
import { AppSettings } from '../appSetting.component';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Ambulance } from '../Models/classes';

@Injectable()
export class AmbulanceService {

  ApiBaseRoute = AppSettings.API_ENDPOINT + "Ambulance";
  constructor(private http: HttpClient) { }

  GetListOfVolunteers(): Observable<Ambulance[]> {
      return this.http.get<Ambulance[]>(`${this.ApiBaseRoute}/GetListOfAmbulances`);
  }
  GetAmbulanceById(id):Observable<Ambulance>{
    return this.http.get<Ambulance>(`${this.ApiBaseRoute}/GetAmbulanceById/${id}`);
  }
  updateAmbulance(ambulance):Observable<boolean>{
    return this.http.post<boolean>(`${this.ApiBaseRoute}/updateAmbulance`,ambulance);
  } 
  CreateNewAmbulance(ambulance):Observable<boolean>{
    console.log("create")
    return this.http.post<boolean>(`${this.ApiBaseRoute}/CreateNewAmbulance`,ambulance);
  }
}
