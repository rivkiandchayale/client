import { Injectable } from '@angular/core';
import { Volunteer, User, District } from '../Models/classes';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { promise, Session } from 'selenium-webdriver';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CurrentUser } from '../Models/CurrentUser.component';
import {AppSettings} from '../appSetting.component'

@Injectable()
export class LoginService {
    CurrentUser: CurrentUser;
    ApiBaseRoute = AppSettings.API_ENDPOINT + "Login";
    constructor(private http: HttpClient) { }

    GetVolunteerByPassword(user: User): Observable<CurrentUser> {
        return this.http.post<CurrentUser>(`${this.ApiBaseRoute}/Login`, user);
    }
}
