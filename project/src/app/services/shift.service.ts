import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Data } from '../Models/data.component'
import { selectShift } from '../Models/selectShift.component';
import { LoginService } from  './login.service';
import {AppSettings} from '../appSetting.component';
@Injectable()
export class ShiftService {

  ApiBaseRoute = AppSettings.API_ENDPOINT + "Shift";
  constructor(private http: HttpClient) { }

  GetData(id:number): Observable<Data> {
    return this.http.get<Data>(`${this.ApiBaseRoute}/GetShiftData/${id}`);
  }


  searchShift(select: selectShift): Observable<object> {
    return this.http.post(`${this.ApiBaseRoute}/SearchShift`, select);
  }

}
