import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../appSetting.component';
import { Observable } from 'rxjs';
import { City } from '../Models/classes';

@Injectable()
export class CityService {
  ApiBaseRoute = AppSettings.API_ENDPOINT + "City";
  constructor(private http: HttpClient) { }

  GetAllCities(): Observable<City[]> {
      return this.http.get<City[]>(`${this.ApiBaseRoute}/GetAllCities`);
  }

}
