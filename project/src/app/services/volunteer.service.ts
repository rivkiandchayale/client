import { Injectable } from '@angular/core';
import { AppSettings } from '../appSetting.component';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { VolunteerListModel } from '../Models/VolunteerListModel.component';
import { Volunteer } from '../Models/classes';


@Injectable()
export class VolunteerService {


  ApiBaseRoute = AppSettings.API_ENDPOINT + "Volunteer";
  constructor(private http: HttpClient) { }

  GetListOfVolunteers(): Observable<Volunteer[]> {
      return this.http.get<Volunteer[]>(`${this.ApiBaseRoute}/GetListOfVolunteers`);
  }
  GetVolunteerById(id):Observable<Volunteer>{
    return this.http.get<Volunteer>(`${this.ApiBaseRoute}/GetVolunteerById/${id}`);
  }

}
