import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Volunteer } from '../../Models/classes';
import { VolunteerService } from '../../services/volunteer.service';

@Component({
  selector: 'app-volunteer-form',
  templateUrl: './volunteer-form.component.html',
  styleUrls: ['./volunteer-form.component.css']
})
export class VolunteerFormComponent implements OnInit {
  // ,private userService:userService
  constructor(private route: ActivatedRoute, private volunteerService: VolunteerService, private router: Router) { }
  id: number;
  volunteer: Volunteer = new Volunteer();
  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.id = +params['id']
        console.log(this.id);
        if (this.id != 0)
          this.volunteerService.GetVolunteerById(this.id).subscribe(x => { this.volunteer = x; })
      }
    );
  }
  updateVolunteer() {
    alert("updateVolunteer")
  }
  addVolunteer() {
    alert("addVolunteer")
  }
  returnToList() {
    this.router.navigate(['/volunteerList']);
  }

}
