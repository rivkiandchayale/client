import { Component, OnInit,ViewChild } from '@angular/core';
import { VolunteerService } from '../../services/volunteer.service';
import { Volunteer } from '../../Models/classes';
// import { SelectItem } from 'primeng/components/common/selectitem';
import { Router } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-volunteer-list',
  templateUrl: './volunteer-list.component.html',
  styleUrls: ['./volunteer-list.component.css']
})
export class VolunteerListComponent implements OnInit {

  constructor(private volunteerService: VolunteerService, private router: Router) { }
  volunteers: Volunteer[];
  displayedColumns: string[] = [ 'Id', 'PrivateId', 'Tz', 'FirstName','LastName','Phone','Email','IsActive','Staff','Branch','District','button'];//Name?

  dataSource: MatTableDataSource<Volunteer>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.getVolunteers();
    this.dataSource = new MatTableDataSource(); 
  }
//פונקית סינון של הטבלה
  applyFilter(filterValue: string) {
    // this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getVolunteers() {
    this.volunteerService.GetListOfVolunteers().subscribe(x => {
      console.log(x)
      this.volunteers = x
      this.dataSource = new MatTableDataSource(this.volunteers);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    );
  }

  editVolunteer(Id) {
    this.router.navigate(['/volunteerForm',Id]);
  }
  addVolunteer() {
    this.router.navigate(['/volunteerForm', 0]);
  }
}




