import { IdName } from "../Models/IdName.component";
import { City, ShiftKind, Training, Volunteer } from "../Models/classes";

export class Data{
    Cities:City[];
    Shifts:ShiftKind[];
    Volunteers:IdName[];
    // Trainings:Training[];
}