import { IdName } from "../Models/IdName.component";
import { Data } from "@angular/router/src/config";

export class User {
    Id: number;
    UserName: string;
    Password: string;

    constructor() {
    }

}

export class Volunteer extends User {
    
    PrivateId: number;
    Tz: string;
    FirstName: string;
    LastName: string;
    Phone: string;
    Email: string;
    IsActive: boolean;
    Branch:Branch;
    Staff:Staff;
    District:District;

    constructor() {
        super()
    }

}

export class District {
    Id: number;
    Name: District;
    constructor() {

    }
}

export class Ambulance extends User {
    CodeAmbulance: number;
    IsActive: boolean;
    ICU: boolean;
    City:City;
    constructor() {
        super()
    }
}

export class City {
    //idName: IdName;
    Id:string;
    Name:string;
    constructor() {
    }
}
export class Branch {
    idName: IdName;
    constructor() {
    }
}
export class ShiftKind {
    idName: IdName;
    StartHour: Date;
    EndHour: Date;
    constructor() {
    }
}
export class Staff {
    idName: IdName;
    constructor() {
    }
}
export class Training {
    idName: IdName;
    constructor() {
    }
}
export class Permission {
    idName: IdName;
    constructor() {
    }
}
