import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MaterialModule} from './Material';
import { FormsModule } from '@angular/forms';
import { LoginService } from '../app/services/login.service';
import { HttpClientModule } from '@angular/common/http';
import { ShiftService } from '../app/services/shift.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AmbulanceService } from './services/ambulance.service';
import { AmbulanceListComponent } from './ambulance/ambulance-list/ambulance-list.component';
import { AmbulanceFormComponent } from './ambulance/ambulance-form/ambulance-form.component';
import { CityService } from './services/city.service';
import { EventListComponent } from './event/event-list/event-list.component';
import { ErrorComponent } from './error/error.component';
import { EventService } from './services/event.service';
import { VolunteerService } from './services/volunteer.service';
import { ShiftStepperComponent } from './shift/shiftStepper/shiftStepper.component';
import { AssignmentComponent } from './assignment/assignment.component';
import { VolunteerFormComponent } from './volunteer/volunteer-form/volunteer-form.component';
import { EquipmentComponent } from './equipment/equipment.component';
import { ChooseShiftComponent } from './shift/chooseShift/chooseShift.component';
import { EventComponent } from './event/event/event.component';
import { LoginComponent } from './login/login.component';
import { VolunteerListComponent } from './volunteer/volunteer-list/volunteer-list.component';
@NgModule({
  declarations: [
    AppComponent,
    AppComponent,
    LoginComponent,
    EquipmentComponent,
    ChooseShiftComponent,
    EventComponent,
    ShiftStepperComponent,
    AssignmentComponent,
    VolunteerFormComponent,
    VolunteerListComponent,
    AmbulanceListComponent,
    AmbulanceFormComponent,
    EventListComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [LoginService, ShiftService, VolunteerService, AmbulanceService, CityService,EventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
