import { Component, OnInit } from '@angular/core';
import { EventService } from '../../services/event.service';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EventListComponent implements OnInit {

  eventList: any[];

  constructor(private eventService: EventService) { }
  dataSource = this.eventList;
  columnsToDisplay = ['Date', 'Name'];
  expandedElement: any | null;
  ngOnInit() {
    this.eventService.GetListOfEvents()
      .subscribe(data => {
        console.log(data);
        this.eventList = data;
        this.dataSource = this.eventList;
      })
  }

}




