import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AmbulanceService } from '../../services/ambulance.service';
import { Ambulance, City } from '../../Models/classes';
import { CityService } from '../../services/city.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-ambulance-form',
  templateUrl: './ambulance-form.component.html',
  styleUrls: ['./ambulance-form.component.css']
})
export class AmbulanceFormComponent implements OnInit {
  id: number
  ambulance: Ambulance
  cities: City[]
  constructor(public snackBar: MatSnackBar, private route: ActivatedRoute, private cityService: CityService, private ambulanceService: AmbulanceService, private router: Router) { }

  ngOnInit() {
    this.cityService.GetAllCities().subscribe(x => { this.cities = x });
    this.route.params.subscribe(
      params => {
        this.id = +params['id']
        if (this.id != 0)
          this.ambulanceService.GetAmbulanceById(this.id).subscribe(x => {
            this.ambulance = x;
            console.log(this.ambulance.City)
          });
        else
          this.ambulance = new Ambulance();
        this.ambulance.IsActive = true;
        this.ambulance.City = new City()
      }
    );
  }
  updateAmbulance() {
    console.log(this.ambulance)
    this.ambulanceService.updateAmbulance(this.ambulance)
    .subscribe(x => this.snackBar.open('פרטי אמבולנס התעדכנו במערכת בהצלחה', null, { duration: 3000 })
    );
  }
  addAmbulance() {
    console.log(this.ambulance)
    this.ambulanceService.CreateNewAmbulance(this.ambulance)
    .subscribe(x => this.snackBar.open('פרטי אמבולנס נקלטו במערכת בהצלחה', null, { duration: 3000 }));
  }
  returnToList() {
    this.router.navigate(['/ambulanceList']);
  }
}
