import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmbulanceFormComponent } from './ambulance-form.component';

describe('AmbulanceFormComponent', () => {
  let component: AmbulanceFormComponent;
  let fixture: ComponentFixture<AmbulanceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmbulanceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmbulanceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
