import { Component, OnInit, ViewChild } from '@angular/core';
import { AmbulanceService } from '../../services/ambulance.service';
import { Ambulance } from '../../Models/classes';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ambulance-list',
  templateUrl: './ambulance-list.component.html',
  styleUrls: ['./ambulance-list.component.css']
})
export class AmbulanceListComponent implements OnInit {

  constructor(private ambulanceSevice: AmbulanceService, private router: Router) { }

  ambulances: Ambulance[]
  displayedColumns: string[] = [ 'Id', 'CodeAmbulance', 'IsActive', 'City.Name','ICU','Password','UserName','button'];//Name?
  dataSource: MatTableDataSource<Ambulance>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.getAmbulances()
    this.dataSource = new MatTableDataSource();
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'City.Name': return item.City.Name;
        default: return item[property];
      }
    };

    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm, key) => {
        return key === 'City' ? currentTerm + data.City.Name : currentTerm + data[key];
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      // Transform the filter by converting it to lowercase and removing whitespace.
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAmbulances() {
    this.ambulanceSevice.GetListOfVolunteers().subscribe(x => {
      this.ambulances = x;
      this.dataSource = new MatTableDataSource(this.ambulances);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    );
  }

  editAmbulance(id) {
    this.router.navigate(['/ambulanceForm', id]);
  }

  deleteAmbulance(x) {
    console.log(x)
  }
  addAmbulance() {
    this.router.navigate(['/ambulanceForm', 0]);
  }
}
